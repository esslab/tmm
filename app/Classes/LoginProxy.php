<?php

namespace App\Classes\Tools;

use App\Exceptions\InvalidCredentialsException;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Foundation\Application;

class LoginProxy
{
    private $user;

    private $client;

    private const HTTP_BACKEND_AUTH = 'http://backend-test.gpn-card.com/Auth';

    public function __construct(Application $app, User $user) {
        $this->user = $user;
        $this->client = new Client();
    }

    /**
     * Attempt to create an access token using user credentials
     *
     * @param $request
     * @return array
     * @throws \App\Exceptions\InvalidCredentialsException
     */
    public function attemptLogin($request)
    {
        try {
            $response = $this->client->post(self::HTTP_BACKEND_AUTH, [
                'form_params' => [
                    'login' => $request->login,
                    'password' => $request->password
                ]
            ]);
        } catch (ServerException $e) {
            throw new InvalidCredentialsException();
        }

        if ($response->getStatusCode() !== 200) {
            throw new InvalidCredentialsException();
        }

        $tokenResult = $this->user->createToken('Personal Access Token');

        $request->session()->put($tokenResult->accessToken, $request->login);

        return [
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ];
    }
}
