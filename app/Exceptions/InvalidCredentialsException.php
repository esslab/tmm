<?php

namespace App\Exceptions;


use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Response;

class InvalidCredentialsException extends HttpException
{

    /**
     * InvalidCredentialsException constructor.
     * @param string $message
     * @param \Exception|null $exception
     */
    public function __construct($message = 'Unauthorized', \Exception $exception = null)
    {
        parent::__construct(Response::HTTP_UNAUTHORIZED, $message, $exception);
    }
}
