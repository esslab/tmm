<?php

namespace App\Services;


class SessionService
{
    public static function put($type, $data)
    {
        return session([$type => $data]);
    }

    public static function get($type)
    {
        return session($type);
    }
}
