<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use SimpleXMLElement;

class TMMService
{
    private const HTTP_BACKEND_AUTH = 'http://backend-test.gpn-card.com/';
    private $client;

    /**
     * TMMService constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param $dataType
     * @param $params
     * @param null $id
     * @return array
     */
    public function getData($dataType, $params, $id = null): array
    {
        try {
            return $this->callApi($dataType, $params, $id);
        } catch (ServerException $e) {
            $data = SessionService::get($dataType . $id);
            return $data ? json_decode($data, true) : [];
        }
    }

    /**
     * @param $dataType
     * @param $params
     * @param null $id
     * @return array
     */
    private function callApi($dataType, $params, $id): array
    {
        $data = $this->client->get(self::HTTP_BACKEND_AUTH . $dataType, [
            'query' => $params
        ])->getBody();
        $data = $this->getArrayFromXML($data);
        SessionService::put($dataType . $id, json_encode($data));
        return $data;
    }

    /**
     * @param $data
     * @return array
     */
    private function getArrayFromXML($data): array
    {
        return json_decode(json_encode(
            (array)new SimpleXMLElement(
                $data
            )),
            TRUE
        )['Response'];
    }

    /**
     * @param $id
     * @return array
     */
    public function getCardDetails($id): array {
        return $this->getData('CardDetail', [
            'id_card' => (int)$id
        ],
            $id
        );
    }

    /**
     * @param $billId
     * @return array
     */
    public function getCards($billId): array {
        $cards = $this->getData('Cards', [
            'id_bill' => $billId
        ]);
        return collect($cards)->shift();

    }

    /**
     * @return array
     */
    public function getBills($login): array {
        $bills = $this->getData('Bills', [
            'login' => $login
        ]);
        return collect($bills)->shift();
    }
}
