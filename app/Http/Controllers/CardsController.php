<?php

namespace App\Http\Controllers;

use App\Exceptions\ForbiddenException;
use App\Http\Requests\GetCardDetailsRequest;
use App\Http\Requests\GetCardsRequest;
use App\Services\TMMService;
use Illuminate\Support\Facades\Auth;

class CardsController extends Controller
{
    /**
     * @param GetCardsRequest $request
     * @param TMMService $TMMService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCards(GetCardsRequest $request, TMMService $TMMService)
    {
        $limit = $request->input('limit', 10);
        $q = $request->q;

        $cards = $TMMService->getCards($request->id_bill);

        $cards = $q ? collect($cards)->find($q): collect($cards);

        return response($cards->paginate($limit));
    }

    /**
     * @param GetCardDetailsRequest $request
     * @param TMMService $TMMService
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ForbiddenException
     */
    public function getCardDetails(GetCardDetailsRequest $request, TMMService $TMMService)
    {
        $cardDetails = $TMMService->getCardDetails($request->id);

        $bills = $TMMService->getBills(Auth::user()->login);

        if (empty($cardDetails) || empty($bills)) {
            return response("Can't fetch any data. Try again later");
        }

        //We need to manage access control and show cards only for own bills
        //TODO refactor
        foreach ($bills as $bill) {
            if ($bill['id'] === $cardDetails['bill_id']) {
                return response($cardDetails);
            }
        }
        throw new ForbiddenException();
    }
}
