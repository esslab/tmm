<?php

namespace App\Http\Controllers;

use App\Http\Requests\BillsRequest;
use App\Services\TMMService;

class BillsController extends Controller
{
    /**
     * @param BillsRequest $request
     * @param TMMService $TMMService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getBills(BillsRequest $request, TMMService $TMMService)
    {
        $limit = $request->input('limit', 10);

        $bills = $TMMService->getBills($request->login);

        return response(collect($bills)->paginate($limit));
    }
}
