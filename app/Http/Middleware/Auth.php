<?php

namespace App\Http\Middleware;

use App\Exceptions\InvalidCredentialsException;
use App\User;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Support\Facades\Auth as AuthFacade;

class Auth extends Authenticate
{
    /**
     * @param Request $request
     * @return bool
     */
    protected function getToken($request): bool
    {
        if ($request->hasHeader('Authorization')) {
            $authHeader = trim(str_replace('Bearer ', '', $request->header('Authorization')));
            if ($request->session()->has($authHeader)) {
                $user = new User();
                $user->login = $request->session()->get($authHeader);
                $user->token = $authHeader;
                AuthFacade::setUser($user);
                return true;
            }
        }
        return false;

    }

    public function handle($request, \Closure $next, ...$guards)
    {
        if ($this->getToken($request)) {
            return $next($request);
        }
        throw new InvalidCredentialsException();
    }
}
