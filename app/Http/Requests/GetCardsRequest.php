<?php

namespace App\Http\Requests;

use App\Services\TMMService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class GetCardsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param TMMService $TMMService
     * @return bool
     */
    public function authorize(TMMService $TMMService)
    {
        // TODO: refactor or move to a Policy
        $bills = $TMMService->getBills(Auth::user()->login);
        foreach ($bills as $bill) {
            if ($bill['id'] === $this->id_bill) {
                return true;
            }
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_bill' => 'required'
        ];
    }
}
