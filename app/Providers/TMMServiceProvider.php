<?php

namespace App\Providers;

use App\Services\TMMService;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class TMMServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TMMService::class, function () {
            return new Client;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
