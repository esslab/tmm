<?php

Route::post('/login', 'Auth\LoginController@login');


Route::get('/bills', 'BillsController@getBills')->middleware('auth');
Route::get('/cards', 'CardsController@getCards')->middleware('auth');
Route::get('/cards/{id}', 'CardsController@getCardDetails')->middleware('auth');

